package web.model.temp;

import java.util.List;

/**
 * @ClassName: VoPerm
 * @Description: 用于组装ztree的json
 * @author yangyw
 * @date 2016年4月28日 上午9:53:58
 *
 */
public class VoZtree {
	private Long id;
	private String name;
	private Long pid;
	private boolean checked;
	private List<VoZtree> children;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<VoZtree> getChildren() {
		return children;
	}

	public void setChildren(List<VoZtree> children) {
		this.children = children;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public VoZtree(Long id, String name, Long pid) {
		super();
		this.id = id;
		this.name = name;
		this.pid = pid;
	}


}
