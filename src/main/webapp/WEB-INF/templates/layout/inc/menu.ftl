 <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${ctx }/admin/index">JFinal-Base-Admin</a>
            </div>
            <!-- /.navbar-header -->

           <div class="navbar-header pull-right" role="navigation">
		<div style="color: #fff; height: 45px; line-height: 45px; margin-right: 50px;">
			欢迎光临,${username} <a href="${ctx}/admin/loginout" style="color: #fff;"> <i class="fa fa-power-off"></i> 退出
			</a>
		</div>
	</div>
            <!-- /.navbar-top-links -->
	<#include "WEB-INF/templates/layout/common/bpTree.ftl" /> 
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                
                    <ul class="nav" id="side-menu">
                 	   <#list menu as tree>
                        <li>
                        	<#if tree.href?has_content>
                        		<a class="menuc" url="${ctx }${tree.href}"  data-addtab="tab_id_${tree.id}"
					title="${tree.name }"  href="javascript:;">${tree.name }</a>
                        	<#else>
	                            <a href="javascript:;">${tree.name } <#if tree.children?has_content><span class="fa arrow"></span></#if></a>
                        	</#if>
                             <#if tree.children?has_content>
							  <ul class="nav nav-second-level">
							      <@bpTree children=tree.children />
							  </ul>
							 </#if>
                        </li>
                        </#list>
                    </ul>
                    
                    
                    
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>