<SCRIPT type="text/javascript">
	var setting = {
		check : {
			enable : true,
			chkStyle : "checkbox",
			chkboxType : {
				"Y" : "ps",
				"N" : "s"
			}
		},
		view : {
			showIcon : false
		},
		data : {
			simpleData : {
				enable : true
			}
		},
		callback : {
			onExpand : cookieSave,
			onCollapse : cookieRemove
		}
	};
	var IDMark_A = "_a";
	var zNodes = ${znodes};
	function showIconForTree(treeId, treeNode) {
		return !treeNode.isParent;
	};
	$(document).ready(function() {
		$.fn.zTree.init($("#treeDemo"), setting, zNodes);
		initExpand("treeDemo");
	});
	$("#saveperm").click(function(){
		var btn = $(this);
		btn.prop("disabled","disabled")
		btn.html("处理中..")
		var treeObj = $.fn.zTree.getZTreeObj("treeDemo");
		var nodes = treeObj.getCheckedNodes(true);
		var str="";
		for(var i=0;i<nodes.length;i++){
			str += nodes[i].id;
			if(!(i===(nodes.length-1))){
				str += ",";
			}
		}
		$("#perms").val(str);
		$.getJSON("${ctx }/admin/saveperm", {"perms":$("#perms").val(),"adminid":$("#adminid").val()}, function(data){
				layer.msg(data.msg);
				btn.removeAttr("disabled")
				btn.html("保存")
			});
	})
	$("#saveroles").click(function(){
		var btn = $(this);
		btn.prop("disabled","disabled")
		btn.html("处理中..")
		$.getJSON("${ctx }/admin/saverole", $( "#saverole" ).serialize(), function(data){
				layer.msg(data.msg);
				btn.removeAttr("disabled")
				btn.html("保存角色")
			});
	});
</SCRIPT>
<div class="form-group">
	<label>选择角色:</label> 
<form name="form1" class="form-horizontal" method="post" action="" id="saverole">
	<#list roles as role> 
		<label class="checkbox-inline"> 
			<#if armap.get(role.id)?has_content>
				<input type="checkbox" name="role" value="${role.id }" checked="checked">${role.name }
			<#else>
				<input type="checkbox" name="role" value="${role.id }">${role.name }
			</#if>
		</label> 
	</#list>
	<br />
	<br />
	<div class="text-center">
		<input type="hidden" name="adminid" id="adminid" value="${adminid}">
		<button type="button" class="btn btn-success" id="saveroles">保存角色</button>
	</div>
</form>
</div>
<hr/>
<div class="left">
	<ul id="treeDemo" class="ztree"></ul>
</div>
<br />
<form name="form1" class="form-horizontal" method="post" action="" id="addperm">
	<div class="text-center">
		<input type="hidden" name="perms" id="perms" >
		<input type="hidden" name="adminid" id="adminid" value="${adminid}">
		<button type="button" class="btn btn-success" id="saveperm">保存权限</button>
	</div>
</form>