/*
Navicat MySQL Data Transfer

Source Server         : jd203.130
Source Server Version : 50720
Source Host           : 117.48.203.130:3306
Source Database       : jfinalinit

Target Server Type    : MYSQL
Target Server Version : 50720
File Encoding         : 65001

Date: 2018-02-08 14:26:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_login_log
-- ----------------------------
DROP TABLE IF EXISTS `admin_login_log`;
CREATE TABLE `admin_login_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `username` varchar(50) NOT NULL,
  `ip` varchar(64) NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=322 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_login_log
-- ----------------------------
INSERT INTO `admin_login_log` VALUES ('292', 'admin', '127.0.0.1', '2017-07-19 14:53:52');
INSERT INTO `admin_login_log` VALUES ('293', 'admin', '127.0.0.1', '2017-07-19 15:12:17');
INSERT INTO `admin_login_log` VALUES ('294', 'admin', '127.0.0.1', '2017-07-19 15:29:22');
INSERT INTO `admin_login_log` VALUES ('295', 'admin', '127.0.0.1', '2017-07-19 15:29:26');
INSERT INTO `admin_login_log` VALUES ('296', 'admin', '127.0.0.1', '2017-07-24 13:59:52');
INSERT INTO `admin_login_log` VALUES ('297', 'admin', '127.0.0.1', '2017-07-24 13:59:55');
INSERT INTO `admin_login_log` VALUES ('298', 'admin', '127.0.0.1', '2017-07-25 15:54:43');
INSERT INTO `admin_login_log` VALUES ('299', 'admin1', '127.0.0.1', '2017-07-25 15:54:46');
INSERT INTO `admin_login_log` VALUES ('300', 'admin', '127.0.0.1', '2017-07-25 15:54:53');
INSERT INTO `admin_login_log` VALUES ('301', 'admin1', '127.0.0.1', '2017-08-21 16:43:47');
INSERT INTO `admin_login_log` VALUES ('302', 'admin1', '127.0.0.1', '2017-08-21 16:43:53');
INSERT INTO `admin_login_log` VALUES ('303', 'admin1', '127.0.0.1', '2017-08-21 16:44:45');
INSERT INTO `admin_login_log` VALUES ('304', 'admin', '127.0.0.1', '2017-08-21 16:44:50');
INSERT INTO `admin_login_log` VALUES ('305', 'admin1', '127.0.0.1', '2017-08-21 16:44:54');
INSERT INTO `admin_login_log` VALUES ('306', 'admin', '127.0.0.1', '2017-08-21 16:45:30');
INSERT INTO `admin_login_log` VALUES ('307', 'admin1', '127.0.0.1', '2017-08-21 17:05:05');
INSERT INTO `admin_login_log` VALUES ('308', 'admin', '127.0.0.1', '2017-08-21 17:05:11');
INSERT INTO `admin_login_log` VALUES ('309', 'admin', '127.0.0.1', '2017-08-21 17:05:19');
INSERT INTO `admin_login_log` VALUES ('310', 'admin1', '127.0.0.1', '2017-08-21 17:05:23');
INSERT INTO `admin_login_log` VALUES ('311', 'admin', '127.0.0.1', '2017-08-21 17:05:28');
INSERT INTO `admin_login_log` VALUES ('312', 'admin1', '127.0.0.1', '2017-08-25 11:38:15');
INSERT INTO `admin_login_log` VALUES ('313', 'admin', '127.0.0.1', '2017-08-25 11:38:20');
INSERT INTO `admin_login_log` VALUES ('314', 'admin1', '127.0.0.1', '2017-08-25 11:38:24');
INSERT INTO `admin_login_log` VALUES ('315', 'admin', '127.0.0.1', '2017-08-25 11:38:32');
INSERT INTO `admin_login_log` VALUES ('316', 'test', '127.0.0.1', '2017-08-25 11:38:44');
INSERT INTO `admin_login_log` VALUES ('317', 'test', '127.0.0.1', '2017-08-25 11:38:50');
INSERT INTO `admin_login_log` VALUES ('318', 'admin', '127.0.0.1', '2018-02-08 13:52:22');
INSERT INTO `admin_login_log` VALUES ('319', 'admin', '127.0.0.1', '2018-02-08 13:52:32');
INSERT INTO `admin_login_log` VALUES ('320', 'admin', '127.0.0.1', '2018-02-08 13:52:43');
INSERT INTO `admin_login_log` VALUES ('321', 'admin', '127.0.0.1', '2018-02-08 14:17:32');

-- ----------------------------
-- Table structure for admin_perm
-- ----------------------------
DROP TABLE IF EXISTS `admin_perm`;
CREATE TABLE `admin_perm` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) unsigned NOT NULL,
  `perm_id` int(11) unsigned NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_perm
-- ----------------------------
INSERT INTO `admin_perm` VALUES ('24', '2', '16', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('23', '2', '15', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('21', '2', '7', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('16', '2', '1', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('26', '2', '8', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('17', '2', '6', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('27', '2', '11', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('20', '2', '20', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('18', '2', '18', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('25', '2', '17', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('28', '2', '12', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('22', '2', '14', '2017-02-17 10:24:58');
INSERT INTO `admin_perm` VALUES ('29', '2', '13', '2017-02-17 10:24:59');
INSERT INTO `admin_perm` VALUES ('19', '2', '19', '2017-02-17 10:24:58');

-- ----------------------------
-- Table structure for admin_role
-- ----------------------------
DROP TABLE IF EXISTS `admin_role`;
CREATE TABLE `admin_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) unsigned NOT NULL,
  `role_id` int(11) unsigned NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_role
-- ----------------------------
INSERT INTO `admin_role` VALUES ('24', '1', '1', '2017-08-21 17:24:20');
INSERT INTO `admin_role` VALUES ('21', '2', '2', '2017-08-21 17:06:43');

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL,
  `email` varchar(150) DEFAULT NULL,
  `group_id` int(4) DEFAULT '4',
  `is_active` tinyint(1) DEFAULT '0',
  `cookise_hash` varchar(255) DEFAULT NULL,
  `last_login_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `last_login_ip` varchar(100) DEFAULT '' COMMENT '最后登录ip',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES ('1', 'admin', 'EjMRb3vEllv6oBXf5vjIWg))', 'admin@163.com', '1', '0', 'r+f7Uok+cQZToLV1mK13Xu))', '2016-11-16 15:08:09', '127.0.0.1', '2016-04-26 10:18:49');
INSERT INTO `admin_user` VALUES ('2', 'test', 'EjMRb3vEllv6oBXf5vjIWg))', null, '4', '0', null, '2016-05-03 15:45:40', '', '2016-05-03 15:33:38');

-- ----------------------------
-- Table structure for dict
-- ----------------------------
DROP TABLE IF EXISTS `dict`;
CREATE TABLE `dict` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `value` varchar(100) NOT NULL DEFAULT '' COMMENT '数据值',
  `label` varchar(100) NOT NULL DEFAULT '' COMMENT '标签名',
  `type` varchar(100) NOT NULL DEFAULT '' COMMENT '类型',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序（升序）',
  `remarks` varchar(255) DEFAULT '' COMMENT '备注信息',
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `sys_dict_value` (`value`) USING BTREE,
  KEY `sys_dict_label` (`label`) USING BTREE,
  KEY `sys_dict_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Records of dict
-- ----------------------------
INSERT INTO `dict` VALUES ('1', '0', '正常', 'del_flag', '0', '', '0', '2017-04-10 22:07:01', '2017-04-20 00:36:09');
INSERT INTO `dict` VALUES ('2', '1', '隐藏', 'del_flag', '0', '', '0', '2017-04-10 22:07:17', '2017-04-10 22:38:32');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT '' COMMENT '名称',
  `permission` varchar(50) NOT NULL DEFAULT '' COMMENT '权限id',
  `remark` varchar(150) NOT NULL DEFAULT '' COMMENT '备注',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `href` varchar(2000) DEFAULT '' COMMENT '链接',
  `is_show` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否菜单显示',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `perm` (`permission`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('14', '增加', 'admin:roles:add', '增加角色', '7', '10', null, '1', '2016-05-03 14:36:09');
INSERT INTO `permissions` VALUES ('1', '管理员管理', 'admin:admin:view', '管理管理员', '0', '0', null, '0', '2016-04-26 10:20:25');
INSERT INTO `permissions` VALUES ('8', '权限树管理', 'admin:perm:list', '权限树列表', '1', '30', '/admin/permissions/list', '0', '2016-04-28 15:54:35');
INSERT INTO `permissions` VALUES ('6', '管理员列表', 'admin:admin:list', '管理员列表', '1', '10', '/admin/list', '0', '2016-04-28 15:52:35');
INSERT INTO `permissions` VALUES ('15', '修改', 'admin:roles:edit', '修改角色', '7', '20', null, '1', '2016-05-03 14:36:41');
INSERT INTO `permissions` VALUES ('16', '删除', 'admin:roles:del', '删除角色', '7', '30', null, '1', '2016-05-03 14:37:55');
INSERT INTO `permissions` VALUES ('13', '删除', 'admin:perm:del', '删除权限', '8', '30', null, '1', '2016-05-03 14:34:53');
INSERT INTO `permissions` VALUES ('17', '权限', 'admin:roles:perm', '权限管理', '7', '40', null, '1', '2016-05-03 14:38:54');
INSERT INTO `permissions` VALUES ('12', '修改', 'admin:perm:edit', '修改权限', '8', '20', null, '1', '2016-05-03 14:34:24');
INSERT INTO `permissions` VALUES ('20', '权限配置', 'admin:admin:perm', '权限配置', '6', '50', null, '1', '2016-05-03 14:41:13');
INSERT INTO `permissions` VALUES ('18', '增加', 'admin:admin:add', '添加管理员', '6', '10', null, '1', '2016-05-03 14:39:41');
INSERT INTO `permissions` VALUES ('11', '增加', 'admin:perm:add', '增加权限', '8', '10', null, '1', '2016-05-03 14:33:52');
INSERT INTO `permissions` VALUES ('7', '角色管理', 'admin:roles:list', '角色的管理', '1', '20', '/admin/roles/list', '0', '2016-04-28 15:53:20');
INSERT INTO `permissions` VALUES ('19', '删除', 'admin:admin:del', '删除管理员', '6', '40', null, '1', '2016-05-03 14:40:37');
INSERT INTO `permissions` VALUES ('28', '字典管理', 'dict:dict:view', '字典管理', '1', '30', '/admin/dict/list', '0', '2017-07-19 15:11:45');

-- ----------------------------
-- Table structure for role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE `role_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) unsigned NOT NULL,
  `permissions_id` int(11) unsigned NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=221 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_permissions
-- ----------------------------
INSERT INTO `role_permissions` VALUES ('100', '2', '17', '2016-05-05 13:31:56');
INSERT INTO `role_permissions` VALUES ('96', '2', '7', '2016-05-05 13:31:56');
INSERT INTO `role_permissions` VALUES ('97', '2', '14', '2016-05-05 13:31:56');
INSERT INTO `role_permissions` VALUES ('220', '1', '28', '2017-08-21 17:20:00');
INSERT INTO `role_permissions` VALUES ('219', '1', '13', '2017-08-21 17:20:00');
INSERT INTO `role_permissions` VALUES ('95', '2', '1', '2016-05-05 13:31:56');
INSERT INTO `role_permissions` VALUES ('218', '1', '12', '2017-08-21 17:20:00');
INSERT INTO `role_permissions` VALUES ('217', '1', '11', '2017-08-21 17:20:00');
INSERT INTO `role_permissions` VALUES ('216', '1', '8', '2017-08-21 17:20:00');
INSERT INTO `role_permissions` VALUES ('99', '2', '16', '2016-05-05 13:31:56');
INSERT INTO `role_permissions` VALUES ('215', '1', '17', '2017-08-21 17:20:00');
INSERT INTO `role_permissions` VALUES ('214', '1', '16', '2017-08-21 17:20:00');
INSERT INTO `role_permissions` VALUES ('213', '1', '15', '2017-08-21 17:20:00');
INSERT INTO `role_permissions` VALUES ('212', '1', '14', '2017-08-21 17:20:00');
INSERT INTO `role_permissions` VALUES ('211', '1', '7', '2017-08-21 17:20:00');
INSERT INTO `role_permissions` VALUES ('210', '1', '20', '2017-08-21 17:20:00');
INSERT INTO `role_permissions` VALUES ('98', '2', '15', '2016-05-05 13:31:56');
INSERT INTO `role_permissions` VALUES ('209', '1', '19', '2017-08-21 17:19:59');
INSERT INTO `role_permissions` VALUES ('208', '1', '18', '2017-08-21 17:19:59');
INSERT INTO `role_permissions` VALUES ('207', '1', '6', '2017-08-21 17:19:59');
INSERT INTO `role_permissions` VALUES ('205', '3', '7', '2017-08-21 17:13:06');
INSERT INTO `role_permissions` VALUES ('204', '3', '1', '2017-08-21 17:13:06');
INSERT INTO `role_permissions` VALUES ('206', '1', '1', '2017-08-21 17:19:59');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '角色名称',
  `remark` varchar(150) NOT NULL DEFAULT '' COMMENT '描述',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin', '超级管理员', '2016-04-27 10:59:07');
INSERT INTO `roles` VALUES ('2', 'department', '部门管理员', '2016-04-27 15:58:27');
INSERT INTO `roles` VALUES ('3', 'company', '公司管理员', '2016-04-27 16:00:17');
SET FOREIGN_KEY_CHECKS=1;
